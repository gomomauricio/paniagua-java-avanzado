/**
 * 
 */
package com.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

import com.dao.DisqueraDao;
import com.entities.Disquera;

/**
 * @author Ing. Mauricio Gonzalez Mondragon
 *
 */
public class DisqueraDDaoImpl implements DisqueraDao {

	// private para que solo se use aqui
	// static para que este siempre accesible
	// final por que va a ser siempre una constante
	private static final EntityManagerFactory ENTITY_MANAGER_FACTORY = Persistence
			.createEntityManagerFactory("persistenceDevPredator");

	@Override
	public void guardar(Disquera disquera) {
		EntityManager em = ENTITY_MANAGER_FACTORY.createEntityManager();
		
		EntityTransaction et = em.getTransaction();
		et.begin(); // iniciamos la transaccion
		
		try {
			em.persist(disquera);
			et.commit(); //aseguramos los datos en la base
		} catch (Exception e) {
			
			if ( et != null) {
				et.rollback();
			}
			
			e.printStackTrace();
		}
		finally {
			em.close();
		}
		

	}

	@Override
	public void actualizar(Disquera disquera) {
		// TODO Auto-generated method stub

	}

	@Override
	public void eliminar(Disquera disquera) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<Disquera> consultar() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Disquera consultarById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
