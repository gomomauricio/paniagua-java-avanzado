/**
 * 
 */
package com.dao.impl;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;

import com.dao.DisqueraDao;
import com.entities.Disquera;

/**
 * @author Ing. Mauricio Gonzalez Mondragon
 *
 */
class DisqueraDDaoImplTest {
	
	
	private DisqueraDao disqueraDAO = new DisqueraDDaoImpl();

	/**
	 * Test method for {@link com.dao.impl.DisqueraDDaoImpl#guardar(com.entities.Disquera)}.
	 */
	@Test
	void testGuardar() {
		Disquera disquera = new Disquera();
		disquera.setDescripcion("MegaForce");
		disquera.setFechaCreacion(LocalDateTime.now());
		disquera.setEstatus(true);
		
		 this.disqueraDAO.guardar(disquera);
	}

	/**
	 * Test method for {@link com.dao.impl.DisqueraDDaoImpl#actualizar(com.entities.Disquera)}.
	 */
	@Test
	void testActualizar() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.dao.impl.DisqueraDDaoImpl#eliminar(com.entities.Disquera)}.
	 */
	@Test
	void testEliminar() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.dao.impl.DisqueraDDaoImpl#consultar()}.
	 */
	@Test
	void testConsultar() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.dao.impl.DisqueraDDaoImpl#consultarById(java.lang.Long)}.
	 */
	@Test
	void testConsultarById() {
		fail("Not yet implemented");
	}

}
