# Código del curso Java Avanzado - Aplicaciones Empresariales Para Empleo
# Instructor
> [Diego Paniagua López](https://www.udemy.com/course/java-avanzado-aplicaciones-empresariales-para-empleo/) 
 

# Udemy
* Java Avanzado - Aplicaciones Empresariales Para Empleo

## Contiene

* Desarrollar una aplicación web empresarial con frameworks  Primefaces, JPA, Spring.
* Realizar transacciones CRUD con Hibernate
* Gestionar aplicaciones con Maven
* Spring Core, Spring JPA Data, Spring Web e integración de Spring con JSF
* WebServices con API Rest 
* Reportes con JasperReports
* Simular Métodos de Pago con PayPal Sandbox API
* Deployar en AWS - Amazon Elastic Beanstalk
* Ambientar base de datos en Amazon RDS
 

## Descripción
     Crear una aplicación web empresarial de una Tienda Musical simulando la compra de albums de música y  alojar la aplicación en la nube con el servicio de Amazon Web Service de Elastic Beanstalk.


---

## Notas
 
~~~
* Repositorio solo en ** Gitlab  **

/************* MAVEN ***********************/
 
 * package ejecuta tambien las pruebas unitarias
 
 
 view port 
 
 <!-- 		se adapta al ancho del dispositivo mobil -->
	<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
 
 
 
http://localhost:8080/project-jsf/principal.xhtml






---
## Código 

`//desarrollo` 
 `public int suma( int x, int y ) { return x + y; }` 

 `//test` 
`@Test`
`public int suma( int x, int y ) {  assertEquals(2, ( x + y ) ); }`


 

---
#### Version  V.0.1 
* **V.1.0**  _>>>_  Iniciando proyecto [  on 11 FEB, 2022 ]  
 
 