package com.controller;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

//import org.apache.logging.log4j.LogManager;
//import org.apache.logging.log4j.Logger;

//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.dto.UsuarioDTO;
 

/**
 * Clase que permite controlar el comportamiento
 * de la pantalla Login
 * 
 * @author Mauricio González Mondragón
 *
 */


 
@ManagedBean(name = "loginCon")  //por defecto toma el nombre de la clase en minusculas
public class LoginController 
{
//	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
//	  private static final Logger logger = LogManager.getLogger(LoginController.class);
	  private static final Logger logger  = LoggerFactory.getLogger(LoginController.class);
//	private static final Logger logger =  Logger.getLogger(LoginController.class);
	
	private String usuario;
	private String password;
	
	
	//manejamos la sesion
	@ManagedProperty("#{sessionController}")
	private  SessionController sessionController;
	
	@PostConstruct
	public void init()
	{
		 
		System.out.println("Iniciando LoginController   . . . ");
//		System.out.println("empleados " + empleados);
	}
	
	
	public void ingresar()
	{
		System.out.println("Entra a Login");
		try {
			logger.info("Usuario: " + usuario + " | Contraseña: " + password);
			if(usuario.equals("2022") && password.equals("2022"))
			{
				try
				{
					UsuarioDTO user = new UsuarioDTO();
						user.setUsuario( this.usuario );
						user.setPassword(this.password);
					
					this.sessionController.setUsuarioDTO(user);
					this.redireccionar("principal.xhtml");
				}
				catch (Exception e) {
					FacesContext.getCurrentInstance().addMessage("frmLogin:txtUsuario", new FacesMessage(FacesMessage.SEVERITY_FATAL, "La página no existe",""));
				}
			}
			else
			{
				FacesContext.getCurrentInstance().addMessage("frmLogin:txtUsuario", new FacesMessage(FacesMessage.SEVERITY_ERROR, "Ususario y/o contraseña no validos",""));
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		System.out.println("Usuario2: " + usuario + " | Contraseña2: " + password);
	}
	
	
	private void redireccionar(String pagina) throws IOException
	{
		ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
		
		ec.redirect(pagina);
		
	}


	public String getUsuario() {
		return usuario;
	}


	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public SessionController getSessionController() {
		return sessionController;
	}


	public void setSessionController(SessionController sessionController) {
		this.sessionController = sessionController;
	}
	
	
	
	

}
