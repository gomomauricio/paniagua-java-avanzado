/**
 * 
 */
package com.spring.test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Properties;

import org.junit.jupiter.api.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.spring.project_spring_desktop.dao.DisqueraDAO;

/**
 * @author 88464
 * 
 * Clase de prueba unitaria que permite reallizar pruebas con el framework SPRING
 *
 */
class SpringDesktopTest {

	@Test
	void test() {
//		fail("Not yet implemented");
		ApplicationContext context= new ClassPathXmlApplicationContext("applicationContex.xml");
		assertNotNull(context);
		
		DisqueraDAO disqueraDAO = (DisqueraDAO) context.getBean( "diqueraDAO" );
		
		assertNotNull(disqueraDAO);
		
		System.out.println("Contexto cargado exitósamente");
		System.out.println(disqueraDAO);
		
		Properties prop = (Properties) context.getBean("properties");
		
		System.out.println( "# " + prop.get("spring-username") );
	
	}

}
