package com.test;

public class TestFacturas 
{
	
	public static void main(String[] args) {
		System.out.println("Inicia ...");
		
		String cadena = "<cfdi:Comprobante Version=\"4.0\" Fecha=\"2022-03-08T18:06:12\" Serie=\"R\" Folio=\"13024289\" Sello=\"\" NoCertificado=\"00001000000504837901\" Certificado=\"\" SubTotal=\"56.00\" Descuento=\"0.00\" FormaPago=\"99\" MetodoPago=\"PPD\" Moneda=\"MXN\" TipoCambio=\"1\" Total=\"64.96\" TipoDeComprobante=\"I\" LugarExpedicion=\"03800\" Exportacion=\"01\" xmlns:add=\"addenda\" xmlns:cfdi=\"http://www.sat.gob.mx/cfd/4\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.sat.gob.mx/cfd/4 http://www.sat.gob.mx/sitio_internet/cfd/4/cfdv40.xsd\">\r\n" + 
				"<cfdi:Emisor Rfc=\"GSE720216JJ6\" Nombre=\"GENERAL DE SEGUROS\" RegimenFiscal=\"601\"/>\r\n" + 
				"<cfdi:Receptor Rfc=\"XAXX010101000\" Nombre=\"JULIO HERRASTI BOLA&#209;OS\" UsoCFDI=\"S01\" DomicilioFiscalReceptor=\"03800\"  RegimenFiscalReceptor=\"616\"  />\r\n" + 
				"<cfdi:Conceptos>\r\n" + 
				"<cfdi:Concepto ClaveProdServ=\"01010101\" Cantidad=\"1\" ClaveUnidad=\"ACT\" Descripcion=\"Venta\" ValorUnitario=\"56.00\" Importe=\"56.00\" Descuento=\"0.00\" ObjetoImp=\"02\"  >\r\n" + 
				"<cfdi:Impuestos>\r\n" + 
				"<cfdi:Traslados>\r\n" + 
				"<cfdi:Traslado Base=\"56.00\" Impuesto=\"002\" TipoFactor=\"Tasa\" TasaOCuota=\"0.160000\" Importe=\"8.96\" />\r\n" + 
				"</cfdi:Traslados>\r\n" + 
				" </cfdi:Impuestos>\r\n" + 
				"</cfdi:Concepto>\r\n" + 
				"</cfdi:Conceptos>\r\n" + 
				"<cfdi:Impuestos TotalImpuestosTrasladados=\"8.96\">\r\n" + 
				"<cfdi:Traslados>\r\n" + 
				"<cfdi:Traslado   Base=\"56.00\"  Impuesto=\"002\" TipoFactor=\"Tasa\" TasaOCuota=\"0.160000\" Importe=\"8.96\"/>\r\n" + 
				"</cfdi:Traslados>\r\n" + 
				"</cfdi:Impuestos>\r\n" + 
				"<cfdi:Addenda>\r\n" + 
				"<GeneralDeSeguros>\r\n" + 
				"<DatosPoliza \r\n" + 
				"NumAddenda= \"1\"\r\n" + 
				"TipoCfdi= \"CFDI\"\r\n" + 
				"TipoComprobante=\"I\"\r\n" + 
				"SucursalEmisora=\"120\"\r\n" + 
				"Ramo=\"710\"\r\n" + 
				"Poliza=\"59990\"\r\n" + 
				"TipoEndoso=\"\"\r\n" + 
				"NumeroEndoso=\"0\"\r\n" + 
				"ClaveCliente=\"1777901\"\r\n" + 
				"CubreDesde=\"2021-11-26\"\r\n" + 
				"CubreHasta=\"2022-11-26\"\r\n" + 
				"Prima=\"56.00\"\r\n" + 
				"Derecho=\"0.00\"\r\n" + 
				"Recargo=\"0.00\"\r\n" + 
				"Iva=\"8.96\"\r\n" + 
				"TipoSeguro=\"OBLIGATORIO AUTOMOVILES\"\r\n" + 
				"Concepto=\"DESCRIPCION: SIENNA XLE LIMITED AUT., 07 OCUP.\"/>\r\n" + 
				"<DatosConcepto>\r\n" + 
				" <Concepto ClaveProdServ=\"84131503\"\r\n" + 
				"Concepto=\"OBLIGATORIO AUTOMOVILES\"\r\n" + 
				"Prima=\"56.00\"\r\n" + 
				"Derecho=\"0.00\"\r\n" + 
				"Recargo=\"0.00\"\r\n" + 
				"ImporteTotal=\"64.96\"/>\r\n" + 
				"</DatosConcepto>\r\n" + 
				"</GeneralDeSeguros>\r\n" + 
				"</cfdi:Addenda>\r\n" + 
				"</cfdi:Comprobante>\r\n" + 
				"";
		
		 
		
		System.out.println("cadena1= \n" + cadena );
		String descripcionC = "OBLIGATORIO AUTOMOVILES\r\n" + 
				" DESCRIPCION: SIENNA XLE LIMITED AUT., 07 OCUP.\r\n" + 
				"MODELO:   2013\r\n" + 
				"SERIE: 5TDYK3DCXDS321372  MOTOR: 2GRE217475";  //extmgm
		
		System.out.println("descripcionC= \n" + descripcionC );
		  cadena = cadena.replace(descripcionC, "Venta");  //extmgm
		  

			System.out.println("------------v---2>" + (cadena.contains( descripcionC)) + "<2-----------");
		  
//		  System.out.println("cadena2= \n" + cadena );
		
		System.out.println("Termina ...");
	}

}
