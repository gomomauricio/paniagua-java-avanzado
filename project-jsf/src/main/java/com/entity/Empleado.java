/**
 * 
 */
package com.entity;

import java.io.Serializable;

/**
 * @author Ing. Mauricio Gonzalez Mondragon
 *
 */

 
public class Empleado implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3745569605849488147L;

	/** Nombre Empleado **/ 
	private String nombre;

	/** Primer Apellido Empleado **/ 
	private String primerApellido;
	
	/** Segundo Apellido Empleado **/ 
	private String segundoApellido;
	
	/** Puesto Empleado **/ 
	private String puesto;
	
	/** Estatus Empleado **/ 
	private Boolean estatus;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public String getPuesto() {
		return puesto;
	}

	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}

	public Boolean getEstatus() {
		return estatus;
	}

	public void setEstatus(Boolean estatus) {
		this.estatus = estatus;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Empleado [nombre=");
		builder.append(nombre);
		builder.append(", primerApellido=");
		builder.append(primerApellido);
		builder.append(", segundoApellido=");
		builder.append(segundoApellido);
		builder.append(", puesto=");
		builder.append(puesto);
		builder.append(", estatus=");
		builder.append(estatus);
		builder.append("]");
		return builder.toString();
	}
	
	
	
	

}
