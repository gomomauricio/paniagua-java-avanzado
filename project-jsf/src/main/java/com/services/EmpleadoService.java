/**
 * 
 */
package com.services;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.entity.Empleado;

/**
 * @author Ing. Mauricio Gonzalez Mondragon
 * 
 * Clase que permite realizar la logica del negocio del empleado.
 *
 */
public class EmpleadoService  implements Serializable {
	
	
/**
	 * 
	 */
	private static final long serialVersionUID = -4451867018022521583L;

/**
 * Metodo que permite consultar la lista de empleados de empresas de TI
 * 
 * @return {@link Empleado}
 */
	public List<Empleado> consultarEmpleados()
	{
		List<Empleado> empleados = new ArrayList<>();
		
		Empleado empleadoIBM = new Empleado();
			empleadoIBM.setNombre("Diego");
			empleadoIBM.setPrimerApellido("Alvarez");
			empleadoIBM.setSegundoApellido("López");
			empleadoIBM.setPuesto("Jr Developer Java");
			empleadoIBM.setEstatus(true);
			
		Empleado empleadoMicrosoft = new Empleado();
				empleadoMicrosoft.setNombre("Andrea");
				empleadoMicrosoft.setPrimerApellido("Gutierrez");
				empleadoMicrosoft.setSegundoApellido("Hernandez");
				empleadoMicrosoft.setPuesto("Jr Developer C#");
				empleadoMicrosoft.setEstatus(true);
				
				
		Empleado empleadoApple = new Empleado();
				empleadoApple.setNombre("Brandon");
				empleadoApple.setPrimerApellido("Ortega");
				empleadoApple.setSegundoApellido("Lee");
				empleadoApple.setPuesto("Jr Developer PHP");
				empleadoApple.setEstatus(true);
				
				
				Empleado empleadoNetflix = new Empleado();
				empleadoNetflix.setNombre("Julio");
				empleadoNetflix.setPrimerApellido("Castro");
				empleadoNetflix.setSegundoApellido("Gutierrez");
				empleadoNetflix.setPuesto("Jr Developer Netflix");
				empleadoNetflix.setEstatus(true);
				
				
				Empleado empleadoHP = new Empleado();
				empleadoHP.setNombre("Pedro");
				empleadoHP.setPrimerApellido("Contreras");
				empleadoHP.setSegundoApellido("Perez");
				empleadoHP.setPuesto("Jr Developer HP");
				empleadoHP.setEstatus(true);
				
				Empleado empleadoAmazon = new Empleado();
				empleadoAmazon.setNombre("Maria");
				empleadoAmazon.setPrimerApellido("Espinoza");
				empleadoAmazon.setSegundoApellido("Martinez");
				empleadoAmazon.setPuesto("Jr Developer Amazon");
				empleadoAmazon.setEstatus(true);
				
				
				
				Empleado empleadoOracle = new Empleado();
				empleadoOracle.setNombre("Jimena");
				empleadoOracle.setPrimerApellido("Dominguez");
				empleadoOracle.setSegundoApellido("Mercado");
				empleadoOracle.setPuesto("Jr Developer Oracle");
				empleadoOracle.setEstatus(true);
				
				
				
				Empleado empleadoDeloitte = new Empleado();
				empleadoDeloitte.setNombre("Estela");
				empleadoDeloitte.setPrimerApellido("Fernandez");
				empleadoDeloitte.setSegundoApellido("Celis");
				empleadoDeloitte.setPuesto("Jr Developer Deloitte");
				empleadoDeloitte.setEstatus(true);
				
				
				Empleado empleadoDisney = new Empleado();
				empleadoDisney.setNombre("Violeta");
				empleadoDisney.setPrimerApellido("Ortega");
				empleadoDisney.setSegundoApellido("Gomez");
				empleadoDisney.setPuesto("Jr Developer Disney");
				empleadoDisney.setEstatus(true);
		
		
		
		
		
		
		
		
				empleados.add(empleadoIBM);
				empleados.add(empleadoMicrosoft);
				empleados.add(empleadoApple);
				empleados.add(empleadoAmazon);
				empleados.add(empleadoDeloitte);
				empleados.add(empleadoDisney);
				empleados.add(empleadoHP);
				empleados.add(empleadoNetflix);
				empleados.add(empleadoOracle);
				
		return empleados;
	}
}
