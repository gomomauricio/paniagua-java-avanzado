package com.dao;

import java.util.List;

import com.entities.Disquera;

/**
 * Interfaz que genera el DAO para las transacciones del CRUD a la tabla de disquera
 * 
 * 
 * @author Ing. Mauricio Gonzalez Mondragon
 *
 */

public interface DisqueraDao {
	
	void guardar(Disquera disquera);
	
	void actualizar(Disquera disquera);

	void eliminar(Disquera disquera);
	
	List<Disquera> consultar();
	
	Disquera consultarById(Long id);
}
