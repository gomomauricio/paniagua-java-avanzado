/**
 * 
 */
package com.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.entity.Empleado;
import com.services.EmpleadoService;

/**
 * @author Ing. Mauricio Gonzalez Mondragon
 *Clase controller que se encarga de procesar la informacion para la pantalla principal.xhtml
 */
@ManagedBean
@ViewScoped
public class PrincipalController  implements Serializable {
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 328725238980303196L;

	private static final Logger log = LoggerFactory.getLogger(PrincipalController.class); 
	
	private List<Empleado> empleados;
	private List<Empleado> empleadosFiltrados;
	
	private EmpleadoService empleadoServ = new EmpleadoService();
	
	@PostConstruct
	public void init()
	{
		log.info("Iniciando PrincipalController 1 . . . ");
		System.out.println("Iniciando PrincipalController 2 . . . ");
		consultarEmpleados();
//		System.out.println("empleados " + empleados);
	}
	
	public void consultarEmpleados() {
		this.empleados = this.empleadoServ.consultarEmpleados();
	}

	public List<Empleado> getEmpleados() {
		return empleados;
	}

	public void setEmpleados(List<Empleado> empleados) {
		this.empleados = empleados;
	}

	public List<Empleado> getEmpleadosFiltrados() {
		return empleadosFiltrados;
	}

	public void setEmpleadosFiltrados(List<Empleado> empleadosFiltrados) {
		this.empleadosFiltrados = empleadosFiltrados;
	}
	
	
	

}
