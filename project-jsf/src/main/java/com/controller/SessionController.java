package com.controller;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.dto.UsuarioDTO;

/**
 * clase que se encarga de mantener informacion del usuario que ingresa al aplicativo en sesion.
 * 
 * @author Ing. Mauricio Gonzalez Mondragon
 *
 */
@ManagedBean
@SessionScoped
public class SessionController {
	
	/** usuario que se mantedra en sesion */
	private UsuarioDTO usuarioDTO;
	
	
	
	@PostConstruct
	public void init()
	{
		System.out.println("Cargando informacion del usuario en la sesion . . . ");
	}
	
	
	
	

	public UsuarioDTO getUsuarioDTO() {
		return usuarioDTO;
	}

	public void setUsuarioDTO(UsuarioDTO usuarioDTO) {
		this.usuarioDTO = usuarioDTO;
	}
	
	
	
	

}
